<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            body
            {
                margin: 0px;
            }
        </style>
    </head>
    <body>
        <div id="blind" style="background-color: black; width: 100%; height: 100%; position: absolute; opacity: 0; filter:alpha(opacity=0);"></div>
        <div style="margin-left: auto; margin-right: auto; width: 100%; text-align: center;">
            <img src="profile.png"/>
        </div>
    </body>
</html>
<script language="javascript" src="../public/scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var interval = random(5, 30)*1000;
        var flashInterval = random(100, 300);
        setInterval(flash, interval);
        //setInterval(function(){if(interval%2 == 0){ fadeIn(flashInterval); }else{ fadeOut(flashInterval); }}, interval);
    });
    function flash()
    {
        var interval = random(100, 300);
        var delayTime = random(1, 3)*1000;
        fadeIn(interval);
        fadeOut(interval-10);
        $('#blind').delay(delayTime);
    }
    function fadeIn(interval)
    {
        var fadeTime = random(1, 3);
        var optStart = random(55, 80);
        for(var i=0; i < fadeTime;i++)
        {
            var opt = optStart/100;
            $('#blind').fadeTo(interval, opt);
            optStart = random(optStart, 80);
        }
    }
    function fadeOut(interval)
    {
        var fadeTime = random(1, 3);
        var optStart = random(0, 50);
        for(var i=0; i < fadeTime;i++)
        {
            var opt = optStart/100;
            $('#blind').fadeTo(interval, opt);
            optStart = random(0, optStart);
        }
    }
    function random(min, max)
    {
        return parseInt(min + Math.floor(Math.random() * max));
    }
</script>