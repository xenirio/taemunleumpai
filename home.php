<?php include('repository.php'); ?>
<?php
$repository = new Repository();
$msg_ids = $repository->random_status_ids();
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="public/css/default.css"/>
        <title>Ded Chongmankong</title>
    </head>
    <body>
        <div id="blind" style="background-color: black; width: 100%; height: 100%; position: absolute; opacity: 0; filter:alpha(opacity=0);"></div>
        <div class="title">
            <div class="inner_title">
                <a href="https://www.facebook.com/ded.chongmankong" target="_blank">
                    <img id="title_profile" src="public/images/title_profile.png" style="margin-left: 540px;" onmouseover="switch_img_profile(1)" onmouseout="switch_img_profile(0)"/>
                </a>
            </div>
        </div>
        <div class="content">
            <div class="box">
                <img src="public/images/profile_info.png"/>
                <div id="canvas_content" style="padding: 23px 0px 23px 16px; min-height: 20px;">
                    <div id="canvas">
                    </div>
                </div>
                <div class="inner_box">
                    <span>Like · Comment · Share</span>
                </div>
                <div class="inner_box" style="min-height: 33px;">
                    <div class="profile_box">
                        <img src="public/images/pic_profile_small.png" style="padding-right: 4px; float: left;" />
                        Ded Chongmankong
                    </div>
                    <div style="vertical-align: top;">
                        Hello World Comment.
                    </div>
                </div>
                <div class="inner_box" style="min-height: 33px;">
                    <img src="public/images/pic_profile_small.png" style="float: left; margin-right: 8px;"/>
                    <div style="vertical-align: top;">
                        <input type="text" style="width: 611px; height: 22px; border: 1px #bfc8d9 solid;"/>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<script language="javascript" src="public/scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
    var STATUS = <?php echo constant('Message::STATUS'); ?>;
    var LINK = <?php echo constant('Message::LINK'); ?>;
    var YOUTUBE = <?php echo constant('Message::YOUTUBE'); ?>;
    var PHOTO = <?php echo constant('Message::PHOTO'); ?>;
    
    var msg_ids = new Array();
<?php foreach ($msg_ids as $id) { ?>
        msg_ids.push(<?php echo $id; ?>);
<?php } ?>
    render_word(0);
    function switch_img_profile(action)
    {
        if(action == 1)
        {
            $("#title_profile").attr("src","public/images/title_profile_hilight.png");
        }
        else
        {
            $("#title_profile").attr("src","public/images/title_profile.png");
        }
    }
    $(document).ready(function() {
        var interval = random(10, 60)*1000;
        var flashInterval = random(200, 600);
        setInterval(flash, interval);
        //setInterval(function(){if(interval%2 == 0){ fadeIn(flashInterval); }else{ fadeOut(flashInterval); }}, interval);
    });
    function flash()
    {
        var interval = random(100, 300);
        var delayTime = random(1, 3)*1000;
        fadeIn(interval);
        fadeOut(interval-10);
        $('#blind').delay(delayTime);
    }
    function fadeIn(interval)
    {
        var fadeTime = random(1, 3);
        var optStart = random(55, 80);
        for(var i=0; i < fadeTime;i++)
        {
            var opt = optStart/100;
            $('#blind').fadeTo(interval, opt);
            optStart = random(optStart, 80);
        }
    }
    function fadeOut(interval)
    {
        var fadeTime = random(1, 3);
        var optStart = random(0, 50);
        for(var i=0; i < fadeTime;i++)
        {
            var opt = optStart/100;
            $('#blind').fadeTo(interval, opt);
            optStart = random(0, optStart);
        }
    }
    function random(min, max)
    {
        return parseInt(min + Math.floor(Math.random() * max));
    }
    function render_word(index)
    {
        var msg_id = msg_ids[index];
        $.get("json_respond.php?id="+msg_id).success(function(data) {
            var msg = jQuery.parseJSON(data);
            if(msg.type == PHOTO)
            {
                var content = "<img src='"+msg.topic+"' width='648' />";
                print(content, index);
            }
            else if(msg.type == YOUTUBE)
            {
                var vdo_id = msg.topic+"?v=2";
                $.get("http://gdata.youtube.com/feeds/api/videos/"+vdo_id+"&alt=jsonc").success(function(vdo_result) {
                    var vdo_img = vdo_result.data.thumbnail.sqDefault;
                    var vdo_title = vdo_result.data.title;
                    var vdo_link = "http://www.youtube.com/watch?v="+msg.topic;
                    
                    var vdo_content = "<div style='float: left; width: 130px;'>";
                    vdo_content += "<img src='" + vdo_img + "' />";
                    vdo_content += "</div>";
                    vdo_content += "<div class='youtube_box'>";
                    vdo_content += "<a target='_blank' href='" + vdo_link + "'>" + vdo_title + "</a><br/>";
                    vdo_content += "<a target='_blank' href='http://www.youtube.com' style='font-size: 10px; font-weight: normal;'>www.youtube.com</a><br/>";
                    vdo_content += vdo_result.data.description;
                    vdo_content += "</div>";
                    $('#canvas').css('min-height', '90px');
                    print(vdo_content, index);
                }).error(function(jqXHR, textStatus, errorThrown) { print(msg.topic, index); });
            }
            else if(msg.type == LINK)
            {
                var content = "<a href='"+msg.topic+"' target='_blank'>"+msg.topic+"</a>";
                print(content, index);
            }
            else
            {
                $('#canvas').css('min-height', '20px');
                print(msg.topic, index);
            }
        }).error(function(jqXHR, textStatus, errorThrown) { print(textStatus, index); });
    }
    function print(content, index)
    {
        $('#canvas').html(content);
        $('#canvas').fadeIn(500, function () {
            $('#canvas').delay(5000).fadeOut(500, function () {
                if(index < msg_ids.length)
                {
                    render_word(index+1);
                }
                else
                {
                    render_word(0);
                }
            });
        }
    );
    }
</script>