<?php
$appId = "284994784845461";
$secret = "046efbf779e9ea6b7815b4f0fe618f64";
//$redirect_url = "http://taemunleumpai.orchestra.io/access_info.php";
$redirect_url = "http://localhost/taemunleumpai/access_info.php";
$config = array();
$config['appId'] = $appId;
$config['secret'] = $secret;

if (isset($_GET['code'])) {
    $code = $_GET['code'];
    $auth_url = "https://graph.facebook.com/oauth/access_token?client_id={$appId}&redirect_uri={$redirect_url}&client_secret={$secret}&code={$code}";
    $response = file_get_contents($auth_url);
    $params = null;
    parse_str($response, $params);
    $access_token = $params["access_token"];
    if ($access_token != "") {
        $user = "ded.chongmankong";
        //$user = "me";
        $limit = "10";
        $url = "https://graph.facebook.com/{$user}/feed?access_token={$access_token}&limit={$limit}";
        $data = file_get_contents($url);
        $info = json_decode($data, true);
    }
} else {
    $auth_url = "https://www.facebook.com/dialog/oauth?client_id={$appId}&redirect_uri={$redirect_url}";
    header("Location: {$auth_url}");
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="public/css/default.css"/>
    </head>
    <body>
        <div id="blind" style="background-color: black; width: 100%; height: 100%; position: absolute; opacity: 0; filter:alpha(opacity=0);"></div>
        <div class="title">
            <div class="inner_title">
                <a href="https://www.facebook.com/ded.chongmankong" target="_blank">
                    <img id="title_profile" src="public/images/title_profile.png" style="margin-left: 540px;" onmouseover="switch_img_profile(1)" onmouseout="switch_img_profile(0)"/>
                </a>
            </div>
        </div>
        <div class="content">
            <div class="box">
                <img src="public/images/profile_info.png"/>
                <div id="canvas" style="padding: 23px 0px 23px 16px;"></div>
                <div class="inner_box">
                    <span>Like · Comment · Share</span>
                </div>
                <div class="inner_box" style="min-height: 33px;">
                    <div class="profile_box">
                        <img src="public/images/pic_profile_small.png" style="padding-right: 4px; float: left;" />
                        Ded Chongmankong
                    </div>
                    <div style="vertical-align: top;">
                        Hello World Comment.
                    </div>
                </div>
                <div class="inner_box" style="min-height: 33px;">
                    <img src="public/images/pic_profile_small.png" style="float: left; margin-right: 8px;"/>
                    <div style="vertical-align: top;">
                        <input type="text" style="width: 611px; height: 22px; border: 1px #bfc8d9 solid;"/>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<script language="javascript" src="public/scripts/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
<?php if (isset($info)) { ?>
        var word = new Array();
    <?php foreach ($info["data"] as $val) { ?>
        <?php if (isset($val["message"])) { ?>
            <?php $msg = str_replace("\n", '', $val["message"]); ?>
            <?php if ($msg != "") { ?>
                                word.push("<?php echo $msg; ?>");
            <?php } ?>
        <?php } ?>
    <?php } ?>
            var limit = word.length;
            render_word(0);
<?php } ?>
    $(document).ready(function() {
        var interval = random(5, 30)*1000;
        var flashInterval = random(100, 300);
        setInterval(flash, interval);
        //setInterval(function(){if(interval%2 == 0){ fadeIn(flashInterval); }else{ fadeOut(flashInterval); }}, interval);
    });
    function flash()
    {
        var interval = random(100, 300);
        var delayTime = random(1, 3)*1000;
        fadeIn(interval);
        fadeOut(interval-10);
        $('#blind').delay(delayTime);
    }
    function fadeIn(interval)
    {
        var fadeTime = random(1, 3);
        var optStart = random(55, 80);
        for(var i=0; i < fadeTime;i++)
        {
            var opt = optStart/100;
            $('#blind').fadeTo(interval, opt);
            optStart = random(optStart, 80);
        }
    }
    function fadeOut(interval)
    {
        var fadeTime = random(1, 3);
        var optStart = random(0, 50);
        for(var i=0; i < fadeTime;i++)
        {
            var opt = optStart/100;
            $('#blind').fadeTo(interval, opt);
            optStart = random(0, optStart);
        }
    }
    function random(min, max)
    {
        return parseInt(min + Math.floor(Math.random() * max));
    }
    function render_word(index)
    {
        $('#canvas').html(word[index]);
        $('#canvas').fadeIn(5000, function () {
            $('#canvas').fadeOut(5000, function () {
                if(index < limit)
                {
                    render_word(index+1);
                }
                else
                {
                    render_word(0);
                }
            });
        }
    );
    }
    function switch_img_profile(action)
    {
        if(action == 1)
        {
            $("#title_profile").attr("src","public/images/title_profile_hilight.png");
        }
        else
        {
            $("#title_profile").attr("src","public/images/title_profile.png");
        }
    }
</script>